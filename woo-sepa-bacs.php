<?php
/*
Plugin Name: Woocommerce SEPA Payments
Description: Payment gateway for SEPA money transfers
Author: Krabbe
Text Domain: woo-sepa-bacs
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

load_plugin_textdomain( 'woo-sepa-bacs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

/**
 * SEPA Payment Gateway.
 *
 * Provides a SEPA Payment Gateway, modification of woocommerce BACS Gateway
 */
add_action('plugins_loaded', 'init_sepa_gateway_class');
function init_sepa_gateway_class(){

	class WC_Gateway_SEPA extends WC_Payment_Gateway {

		public $domain;

		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
			$this->id                 = 'sepa';
			$this->icon               = apply_filters('woocommerce_sepa_icon', '');
			$this->has_fields         = false;
			$this->method_title       = __( 'SEPA', 'woo-sepa-bacs' );
			$this->method_description = __( 'Allows payments using SEPA money transfer.', 'woo-sepa-bacs' );

			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();

			 // Define user set variables
			$this->title        = $this->get_option( 'title' );
			$this->description  = $this->get_option( 'description' );
			$this->instructions = $this->get_option( 'instructions', $this->description );

			// BACS account fields shown on the thanks page and in emails
			$this->account_details = get_option( 'woocommerce_sepa_accounts',
				array(
					array(
						'account_name'   => $this->get_option( 'account_name' ),
						'bank_name'      => $this->get_option( 'bank_name' ),
						'iban'           => $this->get_option( 'iban' ),
						'bic'            => $this->get_option( 'bic' )
					)
				)
			);

			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'save_account_details' ) );
			add_action( 'woocommerce_thankyou_sepa', array( $this, 'thankyou_page' ) );
			// add_action( 'plugins_loaded', array( $this, 'load_sepa_textdomain' ) );

			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}

		/**
		 * Initialise Gateway Settings Form Fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'woo-sepa-bacs' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable SEPA Bank Transfer', 'woo-sepa-bacs' ),
					'default' => 'yes'
				),
				'title' => array(
					'title'       => __( 'Title', 'woo-sepa-bacs' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'woo-sepa-bacs' ),
					'default'     => __( 'SEPA Bank Transfer', 'woo-sepa-bacs' ),
					'desc_tip'    => true,
				),
				'description' => array(
					'title'       => __( 'Description', 'woo-sepa-bacs' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'woo-sepa-bacs' ),
					'default'     => __( 'Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.', 'woo-sepa-bacs' ),
					'desc_tip'    => true,
				),
				'instructions' => array(
					'title'       => __( 'Instructions', 'woo-sepa-bacs' ),
					'type'        => 'textarea',
					'description' => __( 'Instructions that will be added to the thank you page and emails.', 'woo-sepa-bacs' ),
					'default'     => '',
					'desc_tip'    => true,
				),
				'account_details' => array(
					'type'        => 'account_details'
				),
			);
		}

		/**
		 * generate_account_details_html function.
		 */
		public function generate_account_details_html() {
			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc"><?php _e( 'Account Details', 'woo-sepa-bacs' ); ?>:</th>
				<td class="forminp" id="bacs_accounts">
					<table class="widefat wc_input_table sortable" cellspacing="0">
						<thead>
							<tr>
								<th class="sort">&nbsp;</th>
								<th><?php _e( 'Account Name', 'woo-sepa-bacs' ); ?></th>
								<th><?php _e( 'Bank Name', 'woo-sepa-bacs' ); ?></th>
								<th><?php _e( 'IBAN', 'woo-sepa-bacs' ); ?></th>
								<th><?php _e( 'BIC / Swift', 'woo-sepa-bacs' ); ?></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th colspan="7"><a href="#" class="add button"><?php _e( '+ Add Account', 'woo-sepa-bacs' ); ?></a> <a href="#" class="remove_rows button"><?php _e( 'Remove selected account(s)', 'woo-sepa-bacs' ); ?></a></th>
							</tr>
						</tfoot>
						<tbody class="accounts">
							<?php
							$i = -1;
							if ( $this->account_details ) {
								foreach ( $this->account_details as $account ) {
									$i++;

									echo '<tr class="account">
									<td class="sort"></td>
									<td><input type="text" value="' . esc_attr( $account['account_name'] ) . '" name="sepa_account_name[' . $i . ']" /></td>
									<td><input type="text" value="' . esc_attr( $account['bank_name'] ) . '" name="sepa_bank_name[' . $i . ']" /></td>
									<td><input type="text" value="' . esc_attr( $account['iban'] ) . '" name="sepa_iban[' . $i . ']" /></td>
									<td><input type="text" value="' . esc_attr( $account['bic'] ) . '" name="sepa_bic[' . $i . ']" /></td>
									</tr>';
								}
							}
							?>
						</tbody>
					</table>
					<script type="text/javascript">
						jQuery(function() {
							jQuery('#sepa_accounts').on( 'click', 'a.add', function(){

								var size = jQuery('#sepa_accounts tbody .account').size();

								jQuery('<tr class="account">\
									<td class="sort"></td>\
									<td><input type="text" name="sepa_account_name[' + size + ']" /></td>\
									<td><input type="text" name="sepa_bank_name[' + size + ']" /></td>\
									<td><input type="text" name="sepa_iban[' + size + ']" /></td>\
									<td><input type="text" name="sepa_bic[' + size + ']" /></td>\
									</tr>').appendTo('#sepa_accounts table tbody');

								return false;
							});
						});
					</script>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}
		 
		/**
		 * Save account details table
		 */
		public function save_account_details() {
			$accounts = array();

			if ( isset( $_POST['sepa_account_name'] ) ) {

				$account_names   = array_map( 'wc_clean', $_POST['sepa_account_name'] );
				$bank_names      = array_map( 'wc_clean', $_POST['sepa_bank_name'] );
				$ibans           = array_map( 'wc_clean', $_POST['sepa_iban'] );
				$bics            = array_map( 'wc_clean', $_POST['sepa_bic'] );

				foreach ( $account_names as $i => $name ) {
					if ( ! isset( $account_names[ $i ] ) ) {
						continue;
					}

					$accounts[] = array(
						'account_name'   => $account_names[ $i ],
						'bank_name'      => $bank_names[ $i ],
						'iban'           => $ibans[ $i ],
						'bic'            => $bics[ $i ]
					);
				}
			}

			update_option( 'woocommerce_sepa_accounts', $accounts );
		 }

		 /**
		  * Output for the order received page.
		  */
		 public function thankyou_page( $order_id ) {
		 	echo '<section class="woocommerce-sepa-details">';
				if ( $this->instructions ) {
					echo wpautop( wptexturize( wp_kses_post( $this->instructions ) ) );
				}
				$this->bank_details( $order_id );
			echo "</section>";
		 }

		 /**
		  * Add content to the WC emails.
		  *
		  * @access public
		  * @param WC_Order $order
		  * @param bool $sent_to_admin
		  * @param bool $plain_text
		  * @return void
		  */
		 public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
			if ( ! $sent_to_admin && 'sepa' === $order->payment_method && 'on-hold' === $order->status ) {
				if ( $this->instructions ) {
					echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
				}
				$this->bank_details( $order->id );
			}
		 }

		 /**
		  * Get bank details and place into a list format
		  */
		 private function bank_details( $order_id = '' ) {
			if ( empty( $this->account_details ) ) {
				return;
			}

			echo '<h2 class="woocommerce-sepa-heading">' . __( 'Our Bank Details', 'woo-sepa-bacs' ) . '</h2>' . PHP_EOL;

			$sepa_accounts = apply_filters( 'woocommerce_sepa_accounts', $this->account_details );

			if ( ! empty( $sepa_accounts ) ) {
				foreach ( $sepa_accounts as $sepa_account ) {
					echo '<ul class="order_details bacs_details">' . PHP_EOL;

					$sepa_account = (object) $sepa_account;

					// BACS account fields shown on the thanks page and in emails
					$account_fields = apply_filters( 'woocommerce_sepa_account_fields', array(

						'account_name'	=> array(
							'label' => __( 'Account Name', 'woo-sepa-bacs' ),
							'value' => $sepa_account->account_name
						),

						'bank_name'	=> array(
							'label' => __( 'Bank Name', 'woo-sepa-bacs' ),
							'value' => $sepa_account->bank_name
						),

						'iban'			=> array(
							'label' => __( 'IBAN', 'woo-sepa-bacs' ),
							'value' => $sepa_account->iban
						),

						'bic'			=> array(
							'label' => __( 'SWIFT / BIC', 'woo-sepa-bacs' ),
							'value' => $sepa_account->bic
						)
					), $order_id );

					foreach ( $account_fields as $field_key => $field ) {
						if ( ! empty( $field['value'] ) ) {
							echo '<li class="' . esc_attr( $field_key ) . '"><span class="label">' . esc_attr( $field['label'] ) . ':</span> <span class="value"><strong>' . wptexturize( $field['value'] ) . '</strong></span></li>' . PHP_EOL;
						}
					}

					echo '</ul>';
				}
			}
		 }

		 /**
		  * Process the payment and return the result
		  *
		  * @param int $order_id
		  * @return array
		  */
		 public function process_payment( $order_id ) {

			$order = new WC_Order( $order_id );

			// Mark as on-hold (we're awaiting the payment)
			$order->update_status( 'on-hold', __( 'Awaiting BACS payment', 'woo-sepa-bacs' ) );

			// Reduce stock levels
			$order->reduce_order_stock();

			// Remove cart
			WC()->cart->empty_cart();

			// Return thankyou redirect
			return array(
				'result' 	=> 'success',
				'redirect'	=> $this->get_return_url( $order )
			);
		 }
	 }
 }

 add_filter( 'woocommerce_payment_gateways', 'add_sepa_gateway_class' );
 function add_sepa_gateway_class( $methods ) {
	$methods[] = 'WC_Gateway_SEPA';
	return $methods;
 }
